# R2SCAN-3c calculations

Calculations are done in Orca-5.0.3 with R2SCAN-3c method and mTZVP(default) basis set. For more detaill see 

https://chemrxiv.org/engage/api-gateway/chemrxiv/assets/orp/resource/item/60c752f6bb8c1a21633dbf6c/original/r2scan-3c-an-efficient-swiss-army-knife-composite-electronic-structure-method.pdf

The structures are taken from output of b3lyp-d3bj-6-31g level calculations in MadSchumacherQC directory


