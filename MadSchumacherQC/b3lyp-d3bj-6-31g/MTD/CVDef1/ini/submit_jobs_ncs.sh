#! /bin/bash

inidir=$(pwd)

for ((n=0; n<5; n++)); do
    cd $inidir
    num=$(printf "%05i" $n)
    cd ..
    mkdir $num
    cd $num 
    cp $inidir/batch_ncs.sh      ./
    cp $inidir/orca.template ./
    cp $inidir/mol.eqg       ./
    cp $inidir/cv.def        ./
    sbatch batch_ncs.sh
    cd $inidir
done
